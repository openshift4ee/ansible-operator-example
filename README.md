# What does this operator do?
This is an example operator, which deploys [db_size] replicas of mongo and [web_size] replicas of
mongo express. Take a look at an example yaml at config/samples/cache_v1_mongoansible.yaml  
This operator has been tested on openshift v4.4.

# Requirements
- operator-sdk: https://sdk.operatorframework.io/docs/installation/install-operator-sdk/
- ansible v2.9.0+ (https://docs.ansible.com/ansible/latest/index.html)
- ansible-runner v1.1.0+ (pip install ansible-runner)
- ansible-runner-http v1.0.0 (pip install ansible-runner-http)
- docker v17.03+

# How was the project initialized
- `mkdir operator_name`
- `cd operator_name`
- `operator-sdk init --plugins=ansible --domain=example.com`
- `operator-sdk create api --group cache --version v1 --kind MongoAnsible --generate-role`

# How to build and push the operator image
- `make docker-build docker-push IMG=<some-registry>/<project-name>:<tag>`

# How to run deploy the operator (on a computer with oc installed and logged in)
- `make install`
- `make deploy IMG=<some-registry>/<project-name>:<tag>`

After the image have been deployed, you now have a new Custom Resource named
with the kind you specified above, for example of a usage of this specific operator custom resource,
go to config/samples/cache_v1_mongoansible.yaml 

# How to deploy sample operator custom resource
- `oc apply -f config/samples/cache_v1_mongoansible.yaml`

After this deployment, a mongo service will be up with two instances of mongo express
 linked to that mongo.